<?php
class OemDirect_ChildUpdater_Model_Observer {

    public function updater($observer) {
		
		$order = $observer->getEvent()->getOrder();

		if( $order->getId() ) {
			foreach( $order->getAllVisibleItems() as $item ) {
				$productId = $item->getProductId();
				
				$product = Mage::getModel('catalog/product')->load($productId);
				
				$product_sku = $product->getSku();
				
				$is_grouped = $product->getData('grouped');
				$master_sku = $product->getData('grouped_sku');
				$quantity = (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();
				

				if ($is_grouped) {
					//update master
					$masterProductId = $product->getIdBySku($master_sku);
					
					$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($masterProductId);
					if ($stockItem->getManageStock()) {
						$stockItem->setQty($quantity);
						$stockItem->setIsInStock((int)($quantity > 0));
						$stockItem->save();
					}
				
					//update children
					$collection = Mage::getModel('catalog/product')->getCollection();
					$collection->addAttributeToSelect('grouped_sku'); 
					$collection->addFieldToFilter(array(
					array('attribute'=>'grouped_sku','eq'=>$master_sku),
					));
					
					foreach ($collection as $product) {
						$sku = $product->getSku();
						$productId = $product->getIdBySku($sku);
						
						$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
							if ($stockItem->getManageStock()) {
								$stockItem->setQty($quantity);
								$stockItem->setIsInStock((int)($quantity > 0));
								$stockItem->save();
							}
					}
				}
				else {
					//update children
					$collection = Mage::getModel('catalog/product')->getCollection();
					$collection->addAttributeToSelect('grouped_sku'); 
					$collection->addFieldToFilter(array(
					array('attribute'=>'grouped_sku','eq'=>$product_sku),
					));
					
					foreach ($collection as $product) {
						$sku = $product->getSku();
						$productId = $product->getIdBySku($sku);
						
						$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
							if ($stockItem->getManageStock()) {
								$stockItem->setQty($quantity);
								$stockItem->setIsInStock((int)($quantity > 0));
								$stockItem->save();
							}
					}
				}	
			}        
		}
    }
}